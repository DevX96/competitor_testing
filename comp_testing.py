import pandas as pd
import requests
import json
from elasticsearch import Elasticsearch
import numpy as np
from tqdm import tqdm
import nltk
import re
import string
from nltk.tokenize import sent_tokenize
from company_details import *

def clean_text(text):
    text = text.lower()   
    text = re.sub("'", '', text)
    text = re.sub("[^\w\s\.]", " ", text)
    text = re.sub(" \d+", " ", text)
    text = re.sub(' +', ' ', text)
    text = text.strip()   
    
    return text

def text_checker(sent):
    new_sent = []
    
    checkers = ['formerly known','headquartered','subsidiary of','based in','founded in']
    for sent_ in sent:
        c = 0
        for check in checkers:
            if check in sent_:
                c =1
        if c == 0:
            new_sent.append(sent_)
    return new_sent


def preprocess_description(text):
    
    proc_text = clean_text(text)    
    proc_text = text_checker(sent_tokenize(proc_text))
    proc_text = " ".join([i for i in proc_text])

    punc_remove = proc_text.translate(str.maketrans("","", string.punctuation))
    digit_remove = re.sub(r'\d+', '', punc_remove)
    non_asc_remove = ''.join([i if ord(i)<128 else '' for i in digit_remove])
    cleaned = ' '.join(non_asc_remove.split()).lower()
    

    return cleaned


elk_search = Elasticsearch(['23.102.26.210'], 
                        http_auth=("elastic", \
                            "73Elk@Uar2020"),
                        scheme="http",
                        port=9200,
                        retry_on_timeout=True)

#es_index = "company_master_data_with_industry_gics_marketcap"
es_index =  "company_master_data_with_hybrid_gics_and_specialties"
secret_key = "@P@>w]128-.>12]'jRq120y|ap.:HF.)Bqawetgagbfshqwetqegh"
api_ip = "http://104.41.201.103:8080/"
api_route = "company_similarity_sent_trans_v7_0_backend"
error_list = []

gics_group_dict = json.load(open('model_db/gics_l4_groups_ccm_v2.json'))
inv_map_dict = json.load(open('model_db/inv_map_dict.json'))
map_dict = json.load(open('model_db/map_dict.json'))


#############
# FIND CCM RESULTS WITH COMPETITOR NAME
#############

def get_ccm_results(company_name, company_description, domain, country, form_gics = None):

    comp_res = {"company_name": company_name, "company_description": company_description, "domain": domain, "country": country,
                "competitors": {}}    
    comp_list = []
    
    if form_gics:
        get_ccm_request = {
            "secret_id":secret_key,
            "request_id":57,
            "data":[
                {
                    "id":"id",
                    "text":company_description,
                    "sent":"True",
                    "cap":"False",
                    "domain":domain,
                    "country":country,
                    "gics_l4": form_gics,
                    "financials":"false",
                    "mnn":"true",
                    "ind":"True",
                    "currency_code":"EUR",
                    "valuation_date": "2018-03-01",
                    }
            ]
        }
    
    else:

        get_ccm_request = {
            "secret_id":secret_key,
            "request_id":57,
            "data":[
                {
                    "id":"id",
                    "text":company_description,
                    "sent":"True",
                    "cap":"False",
                    "domain":domain,
                    "country":country,
                    "financials":"false",
                    "mnn":"True",
                    "ind":"True",
                    "currency_code":"EUR",
                    "valuation_date": "2018-03-01",
                    }
            ]
        }

    api_response = requests.post(api_ip+api_route, json=get_ccm_request).json()

    ccm_ids = api_response["complete_processed_data"][0]["processed_data"]["ccm"]["ids"]

    for ccm_id in ccm_ids:
        es_query = {
                        "query":{
                            "match":{
                                "company_id": ccm_id
                            }
                        }
                    }
        es_result = elk_search.search(index=es_index, body=es_query)["hits"]["hits"]
        if es_result:
            comp_name = es_result[0]["_source"]["company_name"]
            comp_desc = es_result[0]["_source"]["description"]
            comp_res["competitors"][comp_name] = comp_desc
            comp_list.append(comp_name)

    return comp_res, comp_list
#############
# FIND Elasticsearch Description Rank
#############

def find_true_company_name(search_query):
    es_query = {
                        "query":{
                            "match":{
                                "company_name": search_query
                            }
                        }
                    }
    es_results = elk_search.search(index=es_index, body=es_query, size = 1)["hits"]["hits"]
    if es_results:
        comp_name = es_results[0]["_source"]["company_name"]
    else:
        comp_name = ""
    
    return comp_name

def find_company_name_from_id(comp_id):

    es_query = {
                        "query":{
                            "match":{
                                "company_id": comp_id
                            }
                        }
                    }
    
    es_results = elk_search.search(index=es_index, body=es_query, size = 1)["hits"]["hits"]

    if es_results:
        comp_name = es_results[0]["_source"]["company_name"]
    
    else:
        comp_name = ""

    return comp_name

def find_es_rank(company_name_list, subject_description):
    
    
    es_query = {
                        "query":{
                            "match":{
                                "processed_description": subject_description
                            }
                        }
                    }
        
    es_results = elk_search.search(index=es_index, body=es_query, size = 500)["hits"]["hits"]

    comp_name_list = []

    print("Number of ES Results", len(es_results))

    if es_results:

        for result in es_results:

            comp_name = result["_source"]["company_name"]
            comp_name_list.append(comp_name)
    
    ranks = []
    matched_names = []
    
    for company_name in company_name_list:

        true_name = find_true_company_name(company_name)
        
        if true_name in comp_name_list:

            rank = comp_name_list.index(true_name)
            ranks.append(rank)
            matched_names.append(comp_name_list[rank])
        
        else:

            ranks.append(1000000000)
            matched_names.append("")

    
    company_name_dict = {"company_names": company_name_list, "ranks": ranks, "matched_name": matched_names}

    company_name_df = pd.DataFrame(company_name_dict)

    return company_name_df

def find_es_rank_gics_query(company_name_list, subject_description, gics_tag, mode = 'manual_group'):

    
    es_query = {
                "size": 500,
                "query":{
                    "bool":{
                        "must":{
                            "match": {
                                "processed_description": subject_description
                            }
                        },
                        "filter":{
                            "terms":{
                                "hybrid_gics.keyword": gics_group_dict[gics_tag]
                            }
                        }
                    }
                }
            }
    
    es_results = elk_search.search(index=es_index, body=es_query, size = 500)["hits"]["hits"]

    comp_name_list = []

    print("Number of ES Results(gics_query)", len(es_results))

    if es_results:

        for result in es_results:

            comp_name = result["_source"]["company_name"]
            comp_name_list.append(comp_name)
    
    ranks = []
    matched_names = []
    
    for company_name in company_name_list:

        true_name = find_true_company_name(company_name)
        
        if true_name in comp_name_list:

            rank = comp_name_list.index(true_name)
            ranks.append(rank)
            matched_names.append(comp_name_list[rank])
        
        else:

            ranks.append(1000000000)
            matched_names.append("")

    
    company_name_dict = {"company_names": company_name_list, "ranks": ranks, "matched_name": matched_names}

    company_name_df = pd.DataFrame(company_name_dict)

    return company_name_df

def get_mnn_ranks(response,company_name_list, company_name):

    mnn_score = response
    output_name_list = []
    output_scores = []

    opt_dict = {}
    for record in mnn_score:
        output_name_list.append(find_company_name_from_id(record['uuid']))
        output_scores.append(int(record['ml_score']))
        opt_dict[record['uuid']] = find_company_name_from_id(record['uuid'])
    
    output_name_list = np.array(output_name_list)
    sorted_args = np.argsort(-(np.array(output_scores)))
    comp_name_list = list(output_name_list[sorted_args])
    ranks = []
    matched_names = []

    for company_name in company_name_list:

        true_name = find_true_company_name(company_name)
        
        if true_name in comp_name_list:

            rank = comp_name_list.index(true_name)
            ranks.append(rank)
            matched_names.append(comp_name_list[rank])
        
        else:

            ranks.append(1000000000)
            matched_names.append("")

    
    company_name_dict = {"company_names": company_name_list, "ranks": ranks, "matched_name": matched_names}

    company_name_df = pd.DataFrame(company_name_dict)

    return company_name_df

def get_ind_ranks(response,company_name_list, company_name):

    ind_score = response
    output_name_list = []
    output_scores = []

    opt_dict = {}
    for record in ind_score:
        output_name_list.append(find_company_name_from_id(record['uuid']))
        output_scores.append(int(record['keyword_score']))
        opt_dict[record['uuid']] = find_company_name_from_id(record['uuid'])
    
    output_name_list = np.array(output_name_list)
    sorted_args = np.argsort(-(np.array(output_scores)))
    comp_name_list = list(output_name_list[sorted_args])
    ranks = []
    matched_names = []

    for company_name in company_name_list:

        true_name = find_true_company_name(company_name)
        
        if true_name in comp_name_list:

            rank = comp_name_list.index(true_name)
            ranks.append(rank)
            matched_names.append(comp_name_list[rank])
        
        else:

            ranks.append(1000000000)
            matched_names.append("")

    
    company_name_dict = {"company_names": company_name_list, "ranks": ranks, "matched_name": matched_names}

    company_name_df = pd.DataFrame(company_name_dict)

    return company_name_df

###########
# FIND Sentence Transformer Rank
###########

def get_sent_trans_rank(company_name_list, company_name, company_description, domain, country, form_gics, valuation_date = None):

    
    api_request = {
            "secret_id":secret_key,
            "request_id":57,
            "data":[
                {
                    "id":"id",
                    "text":company_description,
                    "sent":"True",
                    "cap":"False",
                    "domain":domain,
                    "country":country,
                    "financials":"false",
                    "gics_l4": form_gics,
                    "mnn":"True",
                    "ind":"True",
                    "currency_code":"EUR",
                    "valuation_date": "2018-03-01",
                    }
            ]
        }

    api_response = requests.post(api_ip+api_route, json=api_request).json()
    print('a')

    #mnn_ranks = get_mnn_ranks(api_response["complete_processed_data"][0]["processed_data"]["mnn"], company_name_list, company_name)

    mnn_ranks_gics = get_mnn_ranks(api_response["complete_processed_data"][0]["processed_data"]["ccm"]["mnn_gics"], company_name_list, company_name)

    ind_ranks_gics = get_ind_ranks(api_response["complete_processed_data"][0]["processed_data"]["ccm"]["industry_score"], company_name_list, company_name)

    ccm_distance_scores = api_response["complete_processed_data"][0]["processed_data"]["ccm"]["distance_score"]
    print('b')
    output_name_list = []
    output_scores = []

    opt_dict = {}
    for record in ccm_distance_scores:
        
        output_name_list.append(find_company_name_from_id(record['uuid']))
        output_scores.append(float(record['score']))
        opt_dict[record['uuid']] = find_company_name_from_id(record['uuid'])
    print('c')
    #print(opt_dict)
    output_name_list = np.array(output_name_list)
    #print(output_name_list)
    sorted_args = np.argsort(-(np.array(output_scores)))

    comp_name_list = list(output_name_list[sorted_args])
    
    ranks = []
    matched_names = []
    
    for company_name in company_name_list:

        true_name = find_true_company_name(company_name)
        
        if true_name in comp_name_list:

            rank = comp_name_list.index(true_name)
            ranks.append(rank)
            matched_names.append(comp_name_list[rank])
        
        else:

            ranks.append(1000000000)
            matched_names.append("")

    
    company_name_dict = {"company_names": company_name_list, "ranks": ranks, "matched_name": matched_names}

    company_name_df = pd.DataFrame(company_name_dict)

    return list(company_name_df['ranks']), list(mnn_ranks_gics['ranks']),list(ind_ranks_gics['ranks'])

###########
# Convert Gics Tag
###########
def convert_gics(form_gics):
    form_gics = form_gics.upper()
    form_gics = form_gics.replace('AND', '&')
    form_gics = form_gics.replace(' ', '_')
    return form_gics

def testing_func(company_name, company_desc, country, form_gics, domain, expected_company_names_list):
    try:
        output = {}
        print(2)
        results, name_list = get_ccm_results(company_name, company_desc, domain, country, form_gics)
        print(3)
        
        name_list_gt = [find_true_company_name(c) for c in expected_company_names_list]
        
        # Present in AI Response
        ai_response_flag = [1 if i in name_list else 0 for i in name_list_gt]
        
        # BM25 Syntactic Similarity Description Rank
        rank_bm25 = list(find_es_rank(expected_company_names_list, company_desc)['ranks'])
        print(4)
        # BM25 Syntactic Similarity + GICS L4 Group Rank 
        rank_bm25_gics = list(find_es_rank_gics_query(expected_company_names_list, company_desc, form_gics)['ranks'])
        print(5)
        # Sentence Transformer Similarity Score Rank     
        rank_sent_trans, mnn_ranks_gics,ind_ranks_gics  = get_sent_trans_rank(expected_company_names_list, company_name, company_desc, domain, country, form_gics)
        #rank_sent_trans, mnn_ranks, mnn_ranks_gics  = get_sent_trans_rank(expected_company_names_list, company_name, company_desc, domain, country, form_gics)                             
        print(6)

        # Present in Top 200 AI Response
        ai_top200_response_flag = [1 if rank != 1000000000 else 0 for rank in rank_sent_trans]
        #mnn_top200_response_flag = [1 if rank != 1000000000 else 0 for rank in mnn_ranks]
        mnn_gics_top500_response_flag = [1 if rank != 1000000000 else 0 for rank in mnn_ranks_gics]

        ind_top500_response_flag = [1 if rank != 1000000000 else 0 for rank in ind_ranks_gics]

        print(7)
        output['subject_company'] = company_name
        output['expected_company_names_list'] = expected_company_names_list
        output['ai_response_flag'] = ai_response_flag
        output['ai_top200_response_flag'] = ai_top200_response_flag
        #output['mnn_top200_response_flag'] = mnn_top200_response_flag
        output['rank_bm25'] = rank_bm25
        output['rank_bm25_gics_cap_IQ'] = rank_bm25_gics
        output['rank_sent_trans'] = rank_sent_trans
        output['mnn_top200_response_flag'] = mnn_gics_top500_response_flag
        output['ind_top500_response_flag'] = ind_top500_response_flag
        #output['rank_mnn'] = mnn_ranks
        output['rank_mnn_gics'] = mnn_ranks_gics
        output['rank_ind_gics'] = ind_ranks_gics
        print(8)
    
        output['expected_company_names_list'].extend(["a","a"])
        output['ai_response_flag'].extend(["a","a"])
        output['ai_top200_response_flag'].extend(["a","a"])
        #output['mnn_top200_response_flag'] = mnn_top200_response_flag
        output['rank_bm25'].extend(["a","a"])
        output['rank_bm25_gics_cap_IQ'].extend(["a","a"])
        output['rank_sent_trans'].extend(["a","a"])
        output['mnn_top200_response_flag'].extend(["a","a"])
        output['ind_top500_response_flag'].extend(["a","a"])
        #output['rank_mnn'] = mnn_ranks
        output['rank_mnn_gics'].extend(["a","a"])
        output['rank_ind_gics'].extend(["a","a"])

        print(9)
        print(output)        

        return output
    except Exception as e:
        print('-'*20,'\n'*5)
        print(e)
        print('-'*20,'\n'*5)
        error_list.append(company_name)
        print(company_name, "---------", form_gics)
        
        
def final_test_func(company_name_list, company_desc_list, country_list, gics_tag_list, domain_list, list_of_expected_company_names_list):
    
    print(1)
    result = [testing_func(company_name_list[i], company_desc_list[i], country_list[i], gics_tag_list[i], domain_list[i], list_of_expected_company_names_list[i])  for i in tqdm(range(len(company_name_list)))] #len(company_name_list)
    
    return result

if __name__ == '__main__':

    true_gics = [convert_gics(g) for g in gics_tags]

    company_desc_list = [preprocess_description(desc) for desc in company_desc_list]
                            
    result = final_test_func(company_name_list, company_desc_list, country_list, true_gics, domain_list, list_of_expected_company_names_list)                    
    print(10)

    df =  pd.DataFrame.from_records(result)

    df_1 = (df.set_index('subject_company')
              .apply(lambda x: x.apply(pd.Series).stack())
              .reset_index()
             .drop('level_1', 1))
    
    df_1.to_csv('ccm_competitors_v6_200.csv')